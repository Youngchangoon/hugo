## 블로그 개설 공지사항

이 웹사이트는 [GitLab Pages](https://about.gitlab.com/features/pages/) 와 [Hugo](https://gohugo.io)로 1분만에 만들어진 static 웹사이트 입니다.
또한, Markdown 형식으로 어느 곳 에서나 글을 적어보고 싶어 만든 개인적인 블로그입니다!