---
title: C# Extensions Method를 Unity에 활용하기
date: 2018-06-30
tags: ["Unity", "C#"]
---

# C# Extensions Method

C# 3.0부터 추가된 기능이다. 이 기능을 사용하게 되면 기존 형식을 확장, 수정하지 않고도 메소드를 추가할 수 있다. [Microsoft .NET 문서](https://docs.microsoft.com/ko-kr/dotnet/csharp/programming-guide/classes-and-structs/extension-methods) 에선 확장메서드를 호출하나 Extensions Method 나 차이가 없다고 서술하고 있다.

```x
확장명 메서드를 호출하는 것과 형식에 실제로 정의된 메서드를 호출하는 데는 명백한 차이가 없습니다.
```

### 사용용도

이 기능은 어느 클래스에나 사용할 수 있기 때문에 특정한 범위에서 사용하는 것이 좋다. 일반적으로 봉인된(sealed) 클래스에 필요한 기능을 추가할 수 없을 때가 가장 적합한 사용용도 인 거 같다.

### 사용방법

사용방법은 생각보다 간단하다. String 클래스를 예로 들어보겠다.

```C#
namespace ExtensionMethods
{
    public static class MyExtensions
    {
        public static int WordCount(this String str)
        {
            return str.Split(new char[] { ' ', '.', '?' }, 
                             StringSplitOptions.RemoveEmptyEntries).Length;
        }
    }   
}
```

Extensions Method의 생성 조건이 몇 가지 있다.

+ 클래스는 static으로 선언해준다. ( 자동으로 함수도 static )
+ 매개변수의 첫 번째는 this와 해당 메소드가 들어갈 타입을 적어준다.

예제에 맞게 적고 string 변수를 확인해보면 WorldCount 라는 함수가 보일 것이다.

### 기타 설명 
더 자세한건 [Microsoft .NET 문서](https://docs.microsoft.com/ko-kr/dotnet/csharp/programming-guide/classes-and-structs/extension-methods#general-guidelines) 를 참고하길 바란다.

---

# Unity + Extensions Method

이 기능을 유니티에 가져와 적용해본다면 기존에 불필요하게 했던 작업을 함수 하나로 보기 좋게 정리할 수 있게 된다.

기존에 유니티 기능으로만 사용했을 때 불편한 작업이 있었다.

+ transform.position의 x값만 바꾸고 싶을 때
+ Sprite의 Color중 Alpha 값만 바꾸고 싶을 때
+ 배열에서 랜덤을 뽑고 싶을 때 ( 물론 이건 C#이지만..ㅋ )

등등 상황에 따라 더 있겠지만 저런 상황에 Extensions Method 기능을 사용하면 매우 편리하다.

첫 번째 경우를 가지고 설명하자면 Unity의 Transform 클래스에 Extensions Method를 추가하면 좀 더 코드가 깔끔해지고 보기 편해진다.

TransformExtensions.cs

```C#
using UnityEngine;

public static class TransformExtensions
{
    public static void SetPositionX(this Transform transform, float x)
    {
        var newPosition = new Vector3(x, transform.position.y, transform.position.z);
        transform.position = newPosition;
    }
}
```

Test.cs

```C#
using UnityEngine;

public class Test : MonoBehaviour
{
    private void Start()
    {
        // 이전의 X값 변경 방법
        transform.position = Vector3.zero;
        Debug.Log("[Basic] Prev: " + transform.position);
        transform.position = new Vector3(20, transform.position.y, transform.position.z);
        Debug.Log("[Basic] Curr: " + transform.position);

        // Extensions Method
        transform.position = Vector3.zero;
        Debug.Log("[Extensions] Prev: " + transform.position);
        transform.SetPositionX(20);
        Debug.Log("[Extensions] Curr: " + transform.position);
    }
}
```

# 마무리

Extensions Method는 클래스의 수정이 불가능한 부분들에 의한 반복적인 일들을 줄여줄 수 있는 좋은 기능인 것 같다. 유니티로 개발을 하면서 어쩔 수 없이 반복적이고 불필요한 코드들이 늘어난다면 이 기능을 이용해 리팩토링하는 것도 좋을 것 같다.